import React from 'react'
import './Movie.css'
import children from './children.png'
import new_release from './new.jpg'
import {Col} from "react-bootstrap";


export const Movie = ({movie}) => {

    return <>
        <Col>{movie.name}</Col>
        {
            movie.type === 'CHILDREN' &&
            <Col><img className={"Movie-img"} src={children} alt="Children movie"/></Col>
        }
        {
            movie.type === 'NEW_RELEASE' &&
            <Col><img className={"Movie-img"} src={new_release} alt="New release movie"/></Col>
        }
    </>
}
