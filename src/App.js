import './App.css';
import {MovieList} from "./components/movieList/MovieList";
import 'bootstrap/dist/css/bootstrap.min.css';

global.fetch = () => {
    return Promise.resolve({
        json: () => Promise.resolve([{ name: 'Nemo', type: 'CHILDREN' }, {name: 'Iron Man 22', type: 'NEW_RELEASE'}]),
    })
}
function App() {
  return (
    <div className="App">
      <header className="App-header">
          <MovieList/>
      </header>
    </div>
  );
}

export default App;
